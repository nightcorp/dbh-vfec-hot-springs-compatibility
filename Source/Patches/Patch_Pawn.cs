﻿using DubsBadHygiene;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace DBH_Hotspring_Compatibility
{
    [HarmonyPatch(typeof(Pawn), "Tick")]
    public static class Patch_Pawn
    {
        private static Dictionary<Pawn, Thing> bathingPawnsWithBaths = new Dictionary<Pawn, Thing>();

        public static void AddBathingPawn(Pawn pawn, Thing bath)
        {
            pawn.health.AddHediff(DubDef.Washing);
            bathingPawnsWithBaths.Add(pawn, bath);
        }

        public static void RemoveBathingPawn(Pawn pawn)
        {
            pawn.health.RemoveHediff(pawn.health.hediffSet.GetFirstHediffOfDef(DubDef.Washing));
            if(bathingPawnsWithBaths.ContainsKey(pawn))
            {
                bathingPawnsWithBaths.Remove(pawn);
            }
        }

        [HarmonyPostfix]
        public static void AddHygieneForBathingPawns(Pawn __instance)
        {
            if(!bathingPawnsWithBaths.TryGetValue(__instance, out Thing bath))
            {
                return;
            }
            IncreaseHygiene(__instance, bath);
        }

        private static void IncreaseHygiene(Pawn pawn, Thing bath)
        {
            Need_Hygiene need_Hygiene = pawn.needs?.TryGetNeed<Need_Hygiene>();
            if(need_Hygiene == null)
            {
                return;
            }
            ThingDefExtension_HygieneProvider hygieneExtension = bath.def.GetModExtension<ThingDefExtension_HygieneProvider>();
            if(hygieneExtension == null)
            {
                return;
            }

            need_Hygiene.clean(hygieneExtension.hygienePerTick);
        }
    }
}
