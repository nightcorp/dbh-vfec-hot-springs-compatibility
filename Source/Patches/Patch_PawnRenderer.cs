﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace DBH_Hotspring_Compatibility
{
    [HarmonyPatch]
    public static class Patch_PawnRenderer
    {
        private static HashSet<Pawn> pawnsToIgnoreApparelDrawing = new HashSet<Pawn>();

        public static void DisableApparelDrawingFor(Pawn pawn)
        {
            pawnsToIgnoreApparelDrawing.Add(pawn);
        }

        public static void EnableApparelDrawingFor(Pawn pawn)
        {
            if (pawnsToIgnoreApparelDrawing.Contains(pawn))
            {
                pawnsToIgnoreApparelDrawing.Remove(pawn);
            }
        }

#if v1_3 || v1_4
        [HarmonyPatch(typeof(PawnRenderer), "RenderPawnInternal")]
        [HarmonyPrefix]
        public static void PreventApparelDrawingForFlaggedPawns(Pawn ___pawn, ref PawnRenderFlags flags)
        {
            if (!pawnsToIgnoreApparelDrawing.Contains(___pawn))
            {
                return;
            }
            flags &= ~(PawnRenderFlags.Clothes | PawnRenderFlags.Headgear);
        }
#else
        [HarmonyPatch(typeof(PawnRenderer), "GetDrawParms")]
        [HarmonyPostfix]
        public static void PreventApparelDrawingForFlaggedPawns(Pawn ___pawn, ref PawnDrawParms __result)
        {
            if (!pawnsToIgnoreApparelDrawing.Contains(___pawn))
            {
                return;
            }
            __result.flags &= ~(PawnRenderFlags.Clothes | PawnRenderFlags.Headgear);
        }
#endif
    }
}
