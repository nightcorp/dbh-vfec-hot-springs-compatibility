﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using VFEC.Jobs;
using DubsBadHygiene;
using Verse.AI;

namespace DBH_Hotspring_Compatibility
{
    /// <summary>
    /// This classes abuses Lazy loading to allow for encapsulation of the VFE:C reference. 
    /// 
    /// This means no load folders are needed, but this class MUST NOT be called outside of context that ensures VFE:C is loaded!
    /// </summary>
    internal class Patch_JobDriver_SitInBuilding
    {
        public static void CallPatch(Harmony harmony)
        {
            //Log.Message($"Firing VFE:C compatibility patch");
            harmony.Patch(
                AccessTools.Method(typeof(JobDriver_SitInBuilding), "MakeNewToils"), 
                postfix: new HarmonyMethod(typeof(Patch_JobDriver_SitInBuilding), nameof(Patch_JobDriver_SitInBuilding.InjectWashingHediffToils))
            );
        }

        public static IEnumerable<Toil> InjectWashingHediffToils(IEnumerable<Toil> __result, JobDriver __instance)
        {
            Pawn bather = __instance.pawn;
            Thing bath = __instance.job.targetA.Thing;
            List<Toil> toils = __result.ToList();
            if(toils.Count != 2)
            {
                Log.Error($"{nameof(DBH_Hotspring_Compatibility)} expected the JobDriver_SitInBuilding to have exactly 2 toils, but it has {toils.Count}. The compatibility patch will not work properly, please contact NightmareCorporation.");
                foreach(Toil toil in toils)
                {
                    yield return toil;
                }
                yield break;
            }

            yield return toils[0];  // goto toil

            ToilUtility.DecorateBathingToilInit(toils[1], bather, bath);
            ToilUtility.DecorateBathingToilFinish(toils[1], bather, bath);

            yield return toils[1];
        }
    }
}
