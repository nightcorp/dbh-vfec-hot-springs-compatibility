﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StandaloneHotSpring;
using Verse.AI;
using Verse;

namespace DBH_Hotspring_Compatibility
{
    /// <summary>
    /// This classes abuses Lazy loading to allow for encapsulation of the StandaloneHotSpring reference. 
    /// 
    /// This means no load folders are needed, but this class MUST NOT be called outside of context that ensures StandaloneHotSprings is loaded!
    /// 
    /// Standalone Hotsprings has truly awful code quality. internal JobDriver, bad casing, non-sensical toils. Oh well.
    /// </summary>
    public static class Patch_JobDriver_bathStandaloneHotSpring
    {
        public static void CallPatch(Harmony harmony)
        {
            //Log.Message($"Firing StandaloneHotSpring compatibility patch");
            harmony.Patch(
                AccessTools.Method(AccessTools.TypeByName("StandaloneHotSpring.JobDriver_bathStandaloneHotSpring"), "MakeNewToils"),
                postfix: new HarmonyMethod(typeof(Patch_JobDriver_bathStandaloneHotSpring), nameof(Patch_JobDriver_bathStandaloneHotSpring.InjectWashingHediff))
            );
        }

        /// <remarks>
        /// Holy fucking shit. There are 5 toils. The 4th toil is the "go to spot", but it already has the pawn laying in the hot spring, so they start bathing in toil 4
        /// Then toil 5 is the *actual* bathing toil?? So that's where they stop bathing. Fucking hell.
        /// </remarks>
        public static IEnumerable<Toil> InjectWashingHediff(IEnumerable<Toil> __result, object __instance)
        {
            JobDriver jobDriver = __instance as JobDriver;
            Pawn bather = jobDriver.pawn;
            Thing bath = jobDriver.job.targetA.Thing;

            List<Toil> toils = __result.ToList();

            ToilUtility.DecorateBathingToilInit(toils[toils.Count - 1], bather, bath);
            ToilUtility.DecorateBathingToilFinish(toils[toils.Count - 1], bather, bath);

            foreach(Toil toil in toils)
            {
                yield return toil;
            }
        }
    }
}
