﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StandaloneHotSpring;
using Verse.AI;
using Verse;

namespace DBH_Hotspring_Compatibility
{
    /// <summary>
    /// Kijin.
    /// 
    /// The same exact shit as StandaloneHotSpring. Just take my hand, lead me behind the barn and take me out of my misery, dude.
    /// </summary>
    public static class Patch_JobDriver_bathHotSpring
    {
        public static void CallPatch(Harmony harmony)
        {
            //Log.Message($"Firing Kijin compatibility patch");
            harmony.Patch(
                AccessTools.Method(AccessTools.TypeByName("Kijin3.JobDriver_bathHotSpring"), "MakeNewToils"),
                postfix: new HarmonyMethod(typeof(Patch_JobDriver_bathHotSpring), nameof(Patch_JobDriver_bathHotSpring.InjectWashingHediff))
            );
        }

        public static IEnumerable<Toil> InjectWashingHediff(IEnumerable<Toil> __result, object __instance)
        {
            JobDriver jobDriver = __instance as JobDriver;
            Pawn bather = jobDriver.pawn;
            Thing bath = jobDriver.job.targetA.Thing;

            List<Toil> toils = __result.ToList();

            ToilUtility.DecorateBathingToilInit(toils[toils.Count - 2], bather, bath);
            ToilUtility.DecorateBathingToilFinish(toils[toils.Count - 1], bather, bath);

            foreach(Toil toil in toils)
            {
                yield return toil;
            }
        }
    }
}
