﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StandaloneHotSpring;
using Verse.AI;
using Verse;

namespace DBH_Hotspring_Compatibility
{
    /// <summary>
    /// This classes abuses Lazy loading to allow for encapsulation of the SmallHotSpring reference. 
    /// 
    /// This means no load folders are needed, but this class MUST NOT be called outside of context that ensures StandaloneHotSprings is loaded!
    /// 
    /// Small hot springs continues the bad code practices from its mother mod (Standalone hot springs). Patch logic remains the same
    /// </summary>
    public static class Patch_JobDriver_bathSmallHotSpring
    {
        public static void CallPatch(Harmony harmony)
        {
            //Log.Message($"Firing StandaloneHotSpring compatibility patch");
            harmony.Patch(
                AccessTools.Method(AccessTools.TypeByName("SmallHotSpring.JobDriver_bathSmallHotSpring"), "MakeNewToils"),
                postfix: new HarmonyMethod(typeof(Patch_JobDriver_bathSmallHotSpring), nameof(Patch_JobDriver_bathSmallHotSpring.InjectWashingHediff))
            );
        }

        public static IEnumerable<Toil> InjectWashingHediff(IEnumerable<Toil> __result, object __instance)
        {
            JobDriver jobDriver = __instance as JobDriver;
            Pawn bather = jobDriver.pawn;
            Thing bath = jobDriver.job.targetA.Thing;

            List<Toil> toils = __result.ToList();

            ToilUtility.DecorateBathingToilInit(toils.Last(), bather, bath);
            ToilUtility.DecorateBathingToilFinish(toils.Last(), bather, bath);
            Log.Message($"Added toil logic");

            foreach(Toil toil in toils)
            {
                yield return toil;
            }
        }
    }
}
