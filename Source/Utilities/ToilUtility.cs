﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace DBH_Hotspring_Compatibility
{
    public static class ToilUtility
    {
        public static void DecorateBathingToilInit(Toil toil, Pawn bather, Thing bath)
        {
            toil.AddPreInitAction(() =>
            {
                //Log.Message($"Pawn {bather} started bathing");
                Patch_Pawn.AddBathingPawn(bather, bath);
                Patch_PawnRenderer.DisableApparelDrawingFor(bather);
            });
        }
        public static void DecorateBathingToilFinish(Toil toil, Pawn bather, Thing bath)
        {
            toil.AddFinishAction(() =>
            {
                //Log.Message($"Pawn {bather} finished bathing");
                Patch_Pawn.RemoveBathingPawn(bather);
                Patch_PawnRenderer.EnableApparelDrawingFor(bather);
            });
        }
    }
}
