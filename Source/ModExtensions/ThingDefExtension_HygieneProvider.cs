﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace DBH_Hotspring_Compatibility
{
    public class ThingDefExtension_HygieneProvider : DefModExtension
    {
        public float hygienePerTick = 0.001f;
    }
}
