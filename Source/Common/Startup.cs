﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace DBH_Hotspring_Compatibility
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        const string VFECPackageId = "OskarPotocki.VFE.Classical";
        const string StandaloneHotSpringsPackageId = "balistafreak.StandaloneHotSpring";
        const string KijinPackageId = "ssulunge.KijinRace3";
        const string DBHPackageId = "Dubwise.DubsBadHygiene";
        const string DBHLitePackageId = "Dubwise.DubsBadHygiene.Lite";
        const string SmallHotspringPackageId = "zal.smallhotspring";
        static List<string> dbhMods = new List<string>()
        {
            DBHPackageId,
            DBHLitePackageId,
        };

        static Startup()
        {
            if(!ModLister.AnyFromListActive(dbhMods))
            {
                throw new Exception($"Required mod not installed, you must enable either of these mods:\n{string.Join("\n", dbhMods)}");
            }

            Harmony harmony = new Harmony("DBH_Hotspring_Compatibility");
            harmony.PatchAll();
            if(ModLister.GetActiveModWithIdentifier(VFECPackageId) != null)
            {
                //AccessTools.Method(AccessTools.TypeByName(nameof(Patch_JobDriver_SitInBuilding)), nameof(Patch_JobDriver_SitInBuilding.CallPatch)).Invoke(null, new object[] { harmony });
                Patch_JobDriver_SitInBuilding.CallPatch(harmony);
            }
            if(ModLister.GetActiveModWithIdentifier(StandaloneHotSpringsPackageId) != null)
            {
                Patch_JobDriver_bathStandaloneHotSpring.CallPatch(harmony);
            }
            if(ModLister.GetActiveModWithIdentifier(SmallHotspringPackageId) != null)
            {
                Patch_JobDriver_bathSmallHotSpring.CallPatch(harmony);
            }
            if(ModLister.GetActiveModWithIdentifier(KijinPackageId) != null)
            {
                Patch_JobDriver_bathHotSpring.CallPatch(harmony);
            }
        }
    }
}
